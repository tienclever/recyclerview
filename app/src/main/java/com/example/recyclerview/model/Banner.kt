package com.example.recyclerview.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Banner(
    val id: Int,
    val title: String,
    val imageRes: Int
) : Parcelable