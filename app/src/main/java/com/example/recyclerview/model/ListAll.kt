package com.example.recyclerview.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListAll(
    var banners: ArrayList<Banner>?,
    val header: String?,
    val img: Int?,
    val title: String?,
    val type: Int
) : Parcelable