package com.example.recyclerview.fragment.music

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerview.R
import com.example.recyclerview.base.BaseFragment
import com.example.recyclerview.databinding.FragmentMusicBinding
import com.example.recyclerview.fragment.music.adapter.ListAllAdapter
import com.example.recyclerview.model.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MusicFragment : BaseFragment<FragmentMusicBinding>(FragmentMusicBinding::inflate) {
    private val viewModel: MusicFragmentViewModel by viewModels()

    override fun initView() {
        viewModel.getListData()

        val listAllAdapter = ListAllAdapter()
        listAllAdapter.clickItem(object : ListAllAdapter.CallBack {
            override fun clickItemChild(data: Any, childPosition: Int, parentPosition: Int) {
                Toast.makeText(
                    requireContext(),
                    "$childPosition\n $parentPosition\n $data / ",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun clickDeleteChild(
                listDataParent: List<ListAll>,
                positionChild: Int,
                positionParent: Int,
                callBack: CallBackFragment
            ) {
                listDataParent[positionParent].banners?.removeAt(positionChild)
                callBack.updateListbanner(listDataParent[positionParent].banners!!)
                viewModel.updateListData(listDataParent)
            }

            override fun onclickDelete(position: Int, listDataParent: List<ListAll>) {
                val listDataNew = mutableListOf<ListAll>()
                listDataParent.let {
                    listDataNew.addAll(it.toList())
                    listDataNew.removeAt(position)
                }
                listAllAdapter.submitList(listDataNew)
                viewModel.updateListData(listDataNew)
            }

            override fun onclickView(position: Int, data: String?) {
                Toast.makeText(requireContext(), "$position / $data",Toast.LENGTH_SHORT).show()
            }
        })

        binding.rvMusic.apply {
            val gridLayoutManager = GridLayoutManager(requireContext(), 2)
            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (listAllAdapter.getItemViewType(position)) {
                        1 -> 2
                        2 -> 2
                        3 -> 2
                        else -> 1
                    }
                }
            }

            layoutManager = gridLayoutManager
            setHasFixedSize(true)
            adapter = listAllAdapter
        }

        viewModel.liveData.observe(viewLifecycleOwner, Observer {
            listAllAdapter.submitList(it)
        })
    }

    interface CallBackFragment {
        fun updateListbanner(listChild: List<Banner>)
    }
}

