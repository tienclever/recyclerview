package com.example.recyclerview.fragment.home

import androidx.navigation.fragment.findNavController
import com.example.recyclerview.R
import com.example.recyclerview.base.BaseFragment
import com.example.recyclerview.databinding.FragmentHomeBinding

class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    override fun initView() {
        binding.btnStartMusic.setOnClickListener {
            findNavController().navigate(R.id.homToMusic)
        }
        binding.btnStartBook.setOnClickListener {
            findNavController().navigate(R.id.homeToBook)
        }
    }
}