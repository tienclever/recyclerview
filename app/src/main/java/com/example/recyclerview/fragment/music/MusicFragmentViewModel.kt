package com.example.recyclerview.fragment.music

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.recyclerview.model.ListAll
import kotlinx.coroutines.launch

class MusicFragmentViewModel : ViewModel() {
    private val repository: MusicRepository = MusicRepository()
    val liveData = MutableLiveData<List<ListAll>>()

    fun getListData(): List<ListAll> {
        liveData.postValue(repository.getListData())
        return repository.getListData()
    }

    fun setListData() = viewModelScope.launch {
        repository.setListData()
    }

    fun updateListData(listDataParent: List<ListAll>) = viewModelScope.launch {
        val listData = repository.updateListData(listDataParent)
        liveData.postValue(listData)
    }
}