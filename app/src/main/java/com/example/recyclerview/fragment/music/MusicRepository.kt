package com.example.recyclerview.fragment.music

import com.example.recyclerview.R
import com.example.recyclerview.model.*

class MusicRepository {
    private var listData = listOf<ListAll>()

    fun updateListData(listDataParent: List<ListAll>): List<ListAll> {
        listData = listDataParent
        return listData
    }

    init {
        setListData()
    }

    fun setListData() {
        val listBanners = arrayListOf<Banner>()
        for (i in 0..4) {
            val banner = Banner(i, "Banner $i", R.drawable.im_type_1)
            listBanners.add(banner)
        }

        val dataList = arrayListOf<ListAll>()
        val listData1 = ListAll(listBanners, null, null, null, 1)
        val listData2 = ListAll(null, "Music 1", null, null, 2)
        val listData3 = ListAll(null, null, R.drawable.im_type_2, "bai 1", 3)
        val listData4 = ListAll(null, null, R.drawable.im_type_1, "bai 2", 3)
        val listData5 = ListAll(null, null, R.drawable.im_type_3, "bai 3", 3)
        val listData6 = ListAll(null, null, R.drawable.im_type_2, "bai 4", 3)
        val listData7 = ListAll(null, null, R.drawable.im_type_2, "bai 5", 3)
        val listData8 = ListAll(null, "Music 2", null, null, 2)
        val listData9 = ListAll(null, null, R.drawable.im_type_3, "bai 6", 4)
        val listData10 = ListAll(null, null, R.drawable.im_type_2, "bai 7", 4)
        val listData11 = ListAll(null, null, R.drawable.im_type_1, "bai 8", 4)
        val listData12 = ListAll(null, null, R.drawable.im_type_2, "bai 9", 4)
        val listData13 = ListAll(null, null, R.drawable.im_type_2, "bai 10", 4)
        dataList += listData1
        dataList += listData2
        dataList += listData3
        dataList += listData4
        dataList += listData5
        dataList += listData6
        dataList += listData7
        dataList += listData8
        dataList += listData9
        dataList += listData10
        dataList += listData11
        dataList += listData12
        dataList += listData13
        listData = dataList
    }

    fun getListData(): List<ListAll> {
        return listData
    }
}