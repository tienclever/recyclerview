package com.example.recyclerview.fragment.book

import com.example.recyclerview.base.BaseFragment
import com.example.recyclerview.databinding.FragmentBookBinding

class BookFragment : BaseFragment<FragmentBookBinding>(FragmentBookBinding::inflate) {

    override fun initView() {
    }
}