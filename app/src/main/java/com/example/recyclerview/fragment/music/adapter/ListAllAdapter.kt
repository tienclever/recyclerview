package com.example.recyclerview.fragment.music.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.recyclerview.databinding.*
import com.example.recyclerview.fragment.music.MusicFragment
import com.example.recyclerview.model.Banner
import com.example.recyclerview.model.ListAll

class ListAllAdapter : ListAdapter<ListAll, CustomViewHolder>(Companion) {
    private val TYPE1 = 1
    private val TYPE2 = 2
    private val TYPE3 = 3

    lateinit var callback: CallBack
    fun clickItem(callback: CallBack) {
        this.callback = callback
    }

    companion object : DiffUtil.ItemCallback<ListAll>() {
        override fun areItemsTheSame(oldItem: ListAll, newItem: ListAll): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ListAll, newItem: ListAll): Boolean {
            return oldItem == newItem
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        when (viewType) {
            TYPE1 -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemBannerBinding.inflate(inflater, parent, false)
                return CustomViewHolder(binding)
            }
            TYPE2 -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemHeaderBinding.inflate(inflater, parent, false)
                return CustomViewHolder(binding)
            }

            TYPE3 -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemSongOneBinding.inflate(inflater, parent, false)
                return CustomViewHolder(binding)
            }
            else -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemSongTwoBinding.inflate(inflater, parent, false)
                return CustomViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(
        holder: CustomViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        when (holder.binding) {
            is ItemBannerBinding -> {
                var adapter = BannerAdapter()
                adapter.clickItem(object : BannerAdapter.listenner {
                    override fun onclickDelete(positionChild: Int) {
                        callback.clickDeleteChild(
                            currentList,
                            positionChild,
                            position,
                            object : MusicFragment.CallBackFragment {
                                override fun updateListbanner(listChild: List<Banner>) {
                                    adapter.submitList(listChild)
                                }
                            })
                    }

                    override fun onclickView(childPosition: Int, banner: Banner) {
                        callback.clickItemChild(banner, childPosition, position)
                    }
                })
                holder.binding.nestedRecyclerView.adapter = adapter
                getItem(position).banners?.let {
                    adapter.submitList(getItem(position).banners)
                }
                holder.binding.executePendingBindings()
            }

            is ItemHeaderBinding -> {
                val mItem = getItem(position)
                val itemBinding = holder.binding
                itemBinding.lisAll = mItem
                itemBinding.executePendingBindings()

                itemBinding.tvDelete.setOnClickListener {
                    if (::callback.isInitialized) {
                        callback.onclickDelete(holder.adapterPosition, currentList)
                    }
                }

                holder.itemView.setOnClickListener {
                    if (::callback.isInitialized) {
                        callback.onclickView(holder.adapterPosition, mItem.header)
                    }
                }
            }

            is ItemSongOneBinding -> {
                val mItem = getItem(position)
                val itemBinding = holder.binding
                itemBinding.listAll = mItem
                itemBinding.executePendingBindings()

                itemBinding.tvDelete.setOnClickListener {
                    if (::callback.isInitialized) {
                        callback.onclickDelete(holder.adapterPosition, currentList)
                    }
                }

                holder.itemView.setOnClickListener {
                    if (::callback.isInitialized) {
                        callback.onclickView(holder.adapterPosition, getItem(position).title)
                    }
                }
            }

            is ItemSongTwoBinding -> {
                val mItem = getItem(position)
                val itemBinding = holder.binding
                itemBinding.listAll = mItem
                itemBinding.executePendingBindings()

                itemBinding.tvDelete.setOnClickListener {
                    if (::callback.isInitialized) {
                        callback.onclickDelete(holder.adapterPosition, currentList)
                    }
                }

                holder.itemView.setOnClickListener {
                    if (::callback.isInitialized) {
                        callback.onclickView(holder.adapterPosition, mItem.title)
                    }
                }
            }
        }
    }

    interface CallBack {
        fun clickItemChild(data: Any, childPosition: Int, parentPosition: Int)
        fun clickDeleteChild(
            listDataParent: List<ListAll>,
            positionChild: Int,
            positionParent: Int,
            callBack: MusicFragment.CallBackFragment
        )

        fun onclickDelete(position: Int, listDataParent: List<ListAll>)
        fun onclickView(position: Int, data: String?)
    }
}
