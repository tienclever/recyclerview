package com.example.recyclerview.fragment.music.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.databinding.BannerBinding
import com.example.recyclerview.model.Banner

class CustomViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

class BannerAdapter : ListAdapter<Banner, CustomViewHolder>(Companion) {
    lateinit var callback: listenner

    companion object : DiffUtil.ItemCallback<Banner>() {
        override fun areItemsTheSame(oldItem: Banner, newItem: Banner): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Banner, newItem: Banner): Boolean {
            return oldItem.id == newItem.id
        }
    }

    fun clickItem(callback: listenner) {
        this.callback = callback
    }

    override fun submitList(list: List<Banner>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding = BannerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CustomViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val mBanner = getItem(position)
        val itemBinding = holder.binding as BannerBinding
        itemBinding.banner = mBanner

        holder.binding.tvDelete.setOnClickListener {
            if (::callback.isInitialized) {
                callback.onclickDelete(holder.adapterPosition)
            }
        }

        holder.itemView.setOnClickListener {
            if (::callback.isInitialized) {
                callback.onclickView(holder.adapterPosition, getItem(holder.adapterPosition))
            }
        }
        itemBinding.executePendingBindings()
    }

    interface listenner {
        fun onclickDelete(positionChild: Int)
        fun onclickView(childPosition: Int, banner: Banner)
    }
}


