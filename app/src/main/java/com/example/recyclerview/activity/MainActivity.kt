package com.example.recyclerview.activity

import com.example.recyclerview.base.BaseActivity
import com.example.recyclerview.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {
    override fun initView() {
    }
}